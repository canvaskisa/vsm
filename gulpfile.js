const gulp = require('gulp');
const postcss = require('gulp-postcss');
const handlebars = require('gulp-hb');
const rename = require('gulp-rename');
const gutil = require('gulp-util');
const plugins = [
	require('postcss-import')({
		path: ['./src/stylesheets'],
		root: './src/stylesheets'
	}),
	require('postcss-clearfix'),
	require('postcss-short'),
	require('postcss-cssnext')
];

// CSS task.
gulp.task('css', () =>
	gulp.src('src/stylesheets/index.css')
		.pipe(postcss(plugins).on('error', gutil.log))
		.pipe(gulp.dest('./public'))
);

// Templates task.
gulp.task('html', () =>
	gulp.src([
		'src/templates/index/index.hbs',
		'src/templates/contacts/contacts.hbs',
		'src/templates/product/product.hbs',
		'src/templates/orders/orders.hbs',
		'src/templates/checkout/checkout.hbs',
		'src/templates/profile/profile.hbs',
		'src/templates/post/post.hbs',
		'src/templates/sale/sale.hbs',
		'src/templates/sales/sales.hbs',
		'src/templates/recalls/recalls.hbs',
		'src/templates/pricelist/pricelist.hbs',
		'src/templates/cooler/cooler.hbs',
		'src/templates/search/search.hbs',
		'src/templates/sitemap.hbs',
		'src/templates/liked/liked.hbs',
		'src/templates/cart/cart.hbs'
	]).pipe(handlebars({
		partials: 'src/templates/**/*.hbs',
		data: require('./data'),
		helpers: {
			eq: (a, b) => a === b,
			gt: (a, b) => a > b,
			gte: (a, b) => a >= b,
			lt: (a, b) => a < b,
			lte: (a, b) => a <= b
		}
	}).on('error', gutil.log))
		.pipe(rename({extname: '.html'}))
		.pipe(gulp.dest('./public'))
);

// Watch task.
gulp.task('watch', () => {
	const browserSync = require('browser-sync').create();
	const watch = require('gulp-watch');

	browserSync.init({open: false, server: {baseDir: './public'}});

	watch('src/stylesheets/**.css', () => gulp.run('css'));
	watch('src/templates/**.hbs', () => gulp.run('html'));
	watch('public/**/*', browserSync.reload);
});
